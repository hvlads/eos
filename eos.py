import asyncio
import logging
import time
from pprint import pprint
from typing import List, Dict
from aioeos import EosAccount, EosAuthority

import requests
from aiohttp import ClientSession

import eos_exceptions as exceptions
from blockchein_interface import DemoWalletABC

account = EosAccount('testtesttset', private_key='5JCASrztVyVZdb3Uaqzw2GkaJfmbLYHWnbDQB2U4VckGZgmEY5E')
from aioeos import EosJsonRpc, EosTransaction
from aioeos.contracts import eosio_token, eosio

ERROR_NAME_MAP = {
    'exception': exceptions.EosException,
    'deadline_exception': exceptions.EosDeadlineException,
    'action_validate_exception': exceptions.EosActionValidateException,
    'tx_cpu_usage_exceeded': exceptions.EosTxCpuUsageExceededException,
    'tx_net_usage_exceeded': exceptions.EosTxNetUsageExceededException,
    'ram_usage_exceeded': exceptions.EosRamUsageExceededException,
    'eosio_assert_message_exception': exceptions.EosAssertMessageException,
    'block_id_type_exception': exceptions.EosBlockIdTypeException,
}


class EosDemoWallet(DemoWalletABC):
    """
    API endpoints.
    https://developers.eos.io/manuals/eos/latest/nodeos/plugins/chain_api_plugin/api-reference/index

    urls:
    https://eos.greymass.com
    https://mainnet.eoscanada.com
    https://publicapi-mainnet.eosauthority.com
    https://api.cypherglass.com
    https://eu1.eosdac.io
    """

    def __init__(self, url):
        self.url: str = url
        self.request_headers: Dict = {"Content-Type": "application/json"}
        self.last_block: int = 0
        self.last_trx: List = []

    def request(self, url: str, payload=None, method: str = 'post') -> dict:
        payload = payload or {}

        if method == 'post':
            resp_dict = requests.post(url, json=payload, headers=self.request_headers)
        else:
            resp_dict = requests.get(url, params=payload)
        if resp_dict.status_code != 200:
            logging.getLogger('wallet.eth').error('[-] %s', resp_dict.json().get('message'))
            error = resp_dict.json().get('error', {})
            raise ERROR_NAME_MAP.get(
                error.get('name'),
                exceptions.EosException
            )(error)
        return resp_dict.json()

    async def requestAsync(self, url, payload=None) -> dict:
        payload = payload or {}
        async with ClientSession() as session:
            async with session.post(
                    url,
                    json=payload
            ) as res:
                resp_dict = await res.json(content_type=None)
                if resp_dict.get('code') == 500:
                    error = resp_dict.get('error', {})
                    raise ERROR_NAME_MAP.get(
                        error.get('name'),
                        exceptions.EosException
                    )(error)
                return resp_dict

    def isAddress(self, *args, **kwargs) -> bool:
        return True

    async def newAddress(self, name):
        new_account = EosAccount(name=name)
        owner = EosAuthority(
            threshold=1,
            keys=[new_account.key.to_key_weight(1)]
        )

        rpc = EosJsonRpc(url=self.url)
        block = await rpc.get_head_block()

        s = await rpc.sign_and_push_transaction(
            EosTransaction(
                ref_block_num=block['block_num'] & 65535,
                ref_block_prefix=block['ref_block_prefix'],
                actions=[
                    eosio.newaccount(
                        account.name,
                        new_account.name,
                        owner=owner,
                        authorization=[account.authorization('active')]
                    ),
                    eosio.buyrambytes(
                        account.name,
                        new_account.name,
                        2048,
                        authorization=[account.authorization('active')]
                    )
                ],
            ),
            keys=[account.key]
        )
        print(s)
        return s

    def getAddresses(self):
        pass

    def toAmount(self, *args, **kwargs):
        pass

    def fromAmount(self, *args, **kwargs):
        pass

    def getHeight(self):
        return self.request(f"{self.url}/v1/chain/get_info")['head_block_num']

    def getTx(self, txid):
        tr = self.request(f"{self.url}/v1/history/get_transaction",
                          {'id': txid})
        txs = []
        for act in tr['traces'][:10]:
            try:
                io = [
                    {'address': act['act']['data']['from'], 'dir': 'send', 'amount': act['act']['data']['quantity'],
                     'account': act['act']['account'], 'ccy': 'EOS'},
                    {'address': act['act']['data']['to'], 'dir': 'recv', 'amount': act['act']['data']['quantity'],
                     'account': act['act']['account'], 'ccy': 'EOS'},
                ]
                txs.append({
                    'txid': txid,
                    'io': io,
                    'fee': 0,
                    'time': act['block_time'],
                    'confirmations': self.getHeight() - act['block_num']
                })
            except (KeyError, TypeError) as ex:
                logging.getLogger('wallet.eos').error(ex)
        return txs

    def getFeePrice(self):
        """ here are no gas or other transaction fees on EOS """
        pass

    def getBalance(self, account: str, code: str = 'eosio.token', symbol: str = 'EOS'):
        return self.request(
            f"{self.url}/v1/chain/get_currency_balance", {
                'code': code,
                'account': account,
                'symbol': symbol
            })

    def send(self, *args, **kwargs):
        pass

    async def transfer(self, fr, to, amount):
        rpc = EosJsonRpc(url=self.url)
        block = await rpc.get_head_block()

        transaction = EosTransaction(
            ref_block_num=block['block_num'] & 65535,
            ref_block_prefix=block['ref_block_prefix'],
            actions=[
                eosio_token.transfer(
                    from_addr=account.name,
                    to_addr=to,
                    quantity=f'{amount} EOS',
                    authorization=[account.authorization('active')]
                )
            ]
        )
        s = await rpc.sign_and_push_transaction(transaction, keys=[account.key])
        print(s)
        return s

    def getVersion(self):
        pass

    async def processBlock(self, block):
        logging.getLogger('wallet.eos').info('New block %d', block)

    async def processTx(self, block):
        block = await self.get_block(block)
        for tr in block['transactions']:
            logging.getLogger('wallet.eos').info('New tx %s', tr['trx']['id'])

    async def get_block(self, num_block):
        return await self.requestAsync(f'{self.url}/v1/chain/get_block',
                                       payload={"block_num_or_id": num_block})

    async def handler(self):
        logging.getLogger('wallet.eos').info('Eos stream start')
        while True:
            # async with aiohttp.ClientSession() as session:
            info = await self.requestAsync(f'{self.url}/v1/chain/get_info')
            if not self.last_block:
                self.last_block = info['head_block_num']
            else:
                for block in range(self.last_block, info['head_block_num']):
                    asyncio.ensure_future(self.processBlock(block))
                    asyncio.ensure_future(self.processTx(block))
                self.last_block = info['head_block_num']
            time.sleep(3)
            self.last_trx = self.last_trx[:1000]


if __name__ == '__main__':
    # Logging
    sh = logging.StreamHandler()
    logging.getLogger('wallet').addHandler(sh)
    logging.getLogger('wallet').setLevel(logging.DEBUG)

    # wallet = EosDemoWallet(url='https://publicapi-mainnet.eosauthority.com')

    wallet = EosDemoWallet(url='https://eos.greymass.com')
    #wallet = EosDemoWallet(url='http://127.0.0.1:8888')

    # Test
    print('Height', wallet.getHeight())
    print('Balance', wallet.getBalance('testtesttset'))

    pprint(wallet.getTx('942accfcb04ca431d89dda1d8b99e608dbbc6901ac00f374a940f966c6b6466c'))
    #asyncio.run(wallet.newAddress('hvladshvlads')) # 12 symbols
    #asyncio.run(wallet.transfer('testtesttset', 'lemontea1234', 0.0001))

    loop = asyncio.get_event_loop()
    loop.run_until_complete(wallet.handler())
