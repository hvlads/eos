#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import re
import json
import time
import aiohttp
import asyncio
import logging
import requests
import websockets
from decimal import Decimal


class WalletError(Exception):
    pass


class WalletRPCError(Exception):
    def __init__(self, code=0, message=''):
        self.errno = code
        self.message = message

    def __str__(self):
        return 'WalletRPCError (%s: %s)' % (self.errno, self.message)


class WalletInsufficientFunds(Exception):
    def __str__(self):
        return 'Insufficient funds'


class EthereumDemoWallet():
    def __init__(self, url, wsurl, password):
        self.url = url
        self.wsurl = wsurl
        self.password = password

    def request(self, method, params=[]):
        post = {
            'jsonrpc': '2.0',
            'id': int(time.time() * 1000000),
            'method': method,
            'params': params
        }
        r = requests.post(self.url, json=post)
        data = r.json()
        if 'error' in data:
            logging.getLogger('wallet.eth').error('[-] %s', data['error'])
            raise WalletRPCError(data['error']['code'], data['error']['message'])
        return data.get('result')

    async def requestAsync(self, method, params=[]):
        async with aiohttp.ClientSession() as session:
            post = {
                'jsonrpc': '2.0',
                'id': int(time.time() * 1000000),
                'method': method,
                'params': params
            }
            async with session.post(self.url, json=post) as response:
                data = await response.json()
                if 'error' in data:
                    logging.getLogger('wallet.eth').error('[-] %s', data['error'])
                    raise WalletRPCError(data['error']['code'], data['error']['message'])
                return data.get('result')

    def isAddress(self, address):
        return True

    def newAddress(self, amount=None):
        return self.request('personal_newAccount', [self.password])

    def getAddresses(self):
        return self.request('eth_accounts')

    def toAmount(self, amount):
        if isinstance(amount, str):
            amount = int(amount, 16)
        return Decimal(amount) / Decimal('1E18')

    def fromAmount(self, amount):
        return hex(int(Decimal(amount) * Decimal('1E18')))

    def getHeight(self):
        resp = self.request('eth_blockNumber')
        return int(resp, 16)

    def getTx(self, txid):
        tx = self.request('eth_getTransactionByHash', [txid])
        timestamp = None
        confirmations = 0
        amount = self.toAmount(tx['value'])

        if tx['blockHash']:
            block = self.request('eth_getBlockByNumber', [tx['blockNumber'], False])
            timestamp = int(block['timestamp'], 16)
            confirmations = self.getHeight() - int(tx['blockNumber'], 16)

        receipt = self.request('eth_getTransactionReceipt', [txid])
        if receipt is None:
            fee = self.toAmount(int(tx['gas'], 16) * int(tx['gasPrice'], 16))
        else:
            fee = self.toAmount(int(receipt['gasUsed'], 16) * int(tx['gasPrice'], 16))

        io = [
            {'address': tx['from'], 'dir': 'send', 'amount': amount + fee, 'account': None, 'ccy': 'ETH'},
            {'address': tx['to'], 'dir': 'recv', 'amount': amount, 'account': None, 'ccy': 'ETH'},
        ]

        return {
            'txid': txid,
            'io': io,
            'fee': fee,
            'time': timestamp,
            'confirmations': confirmations
        }

    def getFeePrice(self):
        response = self.request('eth_gasPrice')
        return self.toAmount(response)

    def getBalance(self, address):
        resp = self.request('eth_getBalance', [address, 'latest'])
        return self.toAmount(resp)

    def send(self, to, amount=None, includefee=False):
        fr = self.getAddresses()[0]
        return self.transfer(fr, to, amount, includefee)

    def transfer(self, fr, to, amount=None, includefee=False):
        amount = Decimal(amount)
        balance = self.getBalance(fr)

        gasLimit = 21000
        gasPrice = self.getFeePrice()
        fee = gasLimit * gasPrice

        if includefee:
            amount -= fee

        if amount + fee > balance:
            raise WalletInsufficientFunds()

        self.request('personal_unlockAccount', [fr, self.password, 20])
        txid = self.request('eth_sendTransaction', [{
            'from': fr,
            'to': to,
            'value': self.fromAmount(amount),
            'gas': hex(gasLimit),
            'gasPrice': self.fromAmount(gasPrice)
        }])
        return txid

        if amount > 1:
            raise WalletInsufficientFunds()
        return 'txid hash'

    def getVersion(self):
        version = self.request('web3_clientVersion')
        m = re.search('/v(.*?)/', version)
        return m.group(1)

    # Websocket
    async def processBlock(self, block):
        logging.getLogger('wallet.eth').info('New block %d', int(block['number'], 16))

    async def processTx(self, txid):
        # Skip 15/16
        if txid.startswith('0x0'):
            logging.getLogger('wallet.eth').info('New tx %s', txid)

    async def sendWS(self, method, params=[], id=None):
        #
        if id is None:
            id = int(time.time() * 1000000)
        data = json.dumps({
            'id': id,
            'method': method,
            'params': params
        }).encode('utf-8')
        await self.ws.send(data)

    async def handler(self):
        while True:
            try:
                async with websockets.connect(self.wsurl) as self.ws:
                    logging.getLogger('wallet.eth').info('ETH stream start')
                    subs = {}

                    await self.sendWS('eth_subscribe', ['newHeads'], 1)
                    await self.sendWS('eth_subscribe', ['newPendingTransactions'], 2)

                    while True:
                        raw = await self.ws.recv()
                        resp = json.loads(raw)
                        if 'id' in resp:
                            subs[resp['result']] = resp['id']
                            logging.getLogger('wallet.eth').info('Subscribe %d', resp['id'])

                        if 'method' in resp and resp['method'] == 'eth_subscription':
                            if subs.get(resp['params']['subscription']) == 1:
                                asyncio.ensure_future(self.processBlock(resp['params']['result']))
                            if subs.get(resp['params']['subscription']) == 2:
                                asyncio.ensure_future(self.processTx(resp['params']['result']))
            except Exception as e:
                logging.getLogger('wallet.eth').error('Handler %s', e)
                await asyncio.sleep(1)


# Logging
sh = logging.StreamHandler()
logging.getLogger('wallet').addHandler(sh)
logging.getLogger('wallet').setLevel(logging.DEBUG)

# Init
wallet = EthereumDemoWallet(url='http://127.0.0.1:8545', wsurl='ws://127.0.0.1:8546', password='password')

# Test
print('Height', wallet.getHeight())
# print('Address', wallet.newAddress())
# print('Addresses', wallet.getAddresses())
print('Address', wallet.isAddress('0x3f5CE5FBFe3E9af3971dD833D26bA9b5C936f0bE'))
print('Balance', wallet.getBalance('0x3f5CE5FBFe3E9af3971dD833D26bA9b5C936f0bE'))
# print('Send', wallet.send('0x3f5CE5FBFe3E9af3971dD833D26bA9b5C936f0bE', '0.01', includefee=True))
print('Gas price', wallet.getFeePrice())
print('Tx', wallet.getTx('0xe33bd99a6ddaada3200b40166ba00018df1be1305d8206b90a0950eac3e7dd8d'))
print('Tx', wallet.getTx('0x0e31a68853134d06f8e4a5fa7a3b676029fc10d694c021d01c300b9b02d08d1c'))
print('Version', wallet.getVersion())

print('Decode', wallet.toAmount('0x11ed8ec200'))
print('Encode', wallet.fromAmount('7.7E-8'))

# Errors
try:
    wallet.request('eth_blockNumber2')
except Exception as e:
    print(e)

try:
    wallet.send('0x123', '500000000')
except Exception as e:
    print(e)

# Async
loop = asyncio.get_event_loop()
tx = loop.run_until_complete(wallet.handler())
