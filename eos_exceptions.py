class EosException(Exception):
    """Base EOS exception"""


class EosAccountExistsException(EosException):
    """Thrown by create_wallet where account with given name already exists"""


class EosAccountDoesntExistException(EosException):
    """Thrown by get_account where account doesn't exist"""


class EosActionValidateException(EosException):
    """Raised when action payload is invalid"""


class EosMissingTaposFieldsException(EosException):
    """TAPOS fields are missing from Transaction object"""


class EosDeadlineException(EosException):
    """Transaction timed out"""


class EosTxCpuUsageExceededException(EosException):
    """Not enough EOS were staked for CPU"""


class EosTxNetUsageExceededException(EosException):
    """Not enough EOS were staked for NET"""


class EosRamUsageExceededException(EosException):
    """Transaction requires more RAM than what's available on the account"""


class EosAssertMessageException(EosException):
    """
    Generic assertion error from smart contract, can mean literally anything,
    need to parse C++ traceback to figure out what went wrong.
    """


class EosSerializerException(Exception):
    """Base exception class for serializer errors"""


class EosSerializerUnsupportedTypeException(EosSerializerException):
    """Our serializer doesn't support provided object type"""


class EosBlockIdTypeException(EosSerializerException):
    """'Invalid Block number or ID, must be greater than 0 and less than 64 characters'"""


class EosSerializerAbiNameTooLongException(EosSerializerException):
    def __init__(self):
        super().__init__('Value is too long, expected up to 13 characters')


class EosSerializerAbiNameInvalidCharactersException(EosSerializerException):
    def __init__(self):
        super().__init__('Value contains invalid characters')
